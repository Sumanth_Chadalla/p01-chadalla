//
//  ViewController.m
//  HelloWorldz
//
//  Created by Triveni Banpela on 1/22/17.
//  Copyright © 2017 Sumanth Chadalla. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController
@synthesize lblhello;
@synthesize buttonClick;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //lblhello.text=@"Sumanth";
    buttonClick.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    buttonClick.layer.borderWidth=2.0f;
    buttonClick.layer.cornerRadius=10;
    buttonClick.layer.shadowColor=[[UIColor grayColor] CGColor];
    buttonClick.layer.shadowRadius=10;
    lblhello.layer.backgroundColor=[[UIColor clearColor] CGColor];
    lblhello.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    lblhello.layer.borderWidth=1.0f;
    lblhello.layer.cornerRadius=30;
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonClick:(id)sender {
    lblhello.text=@"Hello World!!";
    lblhello.adjustsFontSizeToFitWidth=YES;
    // lblhello.minimumFontSize=0;
   NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
   [dateFormatter setDateFormat:@"hh:mm:ss a"];
   //or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
   // NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *greetings=@"The Time is ";
    NSString *time=[NSString stringWithFormat:[dateFormatter stringFromDate:[NSDate date]]];
   // NSString *final=[greetings stringByAppendingString:time];
   // [self performSelector:@selector(wait) withObject:self afterDelay:3.0 ];
   // [NSThread sleepForTimeInterval:3.0f];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Here your non-main thread.
        [NSThread sleepForTimeInterval:2.0f];
        dispatch_async(dispatch_get_main_queue(), ^{
            //Here you returns to main thread.
              lblhello.text=[greetings stringByAppendingString:time];
            //[self changeText: @"Another text"];
        });
    });

    //lblhello.text=[greetings stringByAppendingString:time];
    
}
- (IBAction)repeatClick:(id)sender {
    
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
   // UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
    self.view.backgroundColor =[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    lblhello.layer.backgroundColor=(__bridge CGColorRef _Nullable)([UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1]);

}
/*
- (void) wait: (NSString*) greet
{
   
}*/


@end
