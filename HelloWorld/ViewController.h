//
//  ViewController.h
//  HelloWorld
//
//  Created by Triveni Banpela on 1/22/17.
//  Copyright © 2017 Sumanth Chadalla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblhello;
@property (weak, nonatomic) IBOutlet UIButton *buttonClick;

@end

