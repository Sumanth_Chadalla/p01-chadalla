//
//  main.m
//  HelloWorld
//
//  Created by Triveni Banpela on 1/22/17.
//  Copyright © 2017 Sumanth Chadalla. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
